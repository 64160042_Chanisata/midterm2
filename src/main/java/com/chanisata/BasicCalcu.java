package com.chanisata;

public class BasicCalcu {
    private double a;
    private double b;
    private double c;
    private double d;
    private double e;
    private double f;
    private double g;

    public BasicCalcu(double a, double b, double c, double d, double e, double f, double g) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
    }

    public double getA(){
        return a;

    }

    public void print() {
        System.out.println("Exemple :");
    }

    // sum
    public String stringSum() {
        return "ANS Sum :";
    }

    public double getSum() {
        return a + b + c + d + e + f + g;
    }


    // subtraction
    public String stringSubtraction() {
        return "ANS Subtraction :";
    }

    public double getSubtraction() {
        return a - b - c - d - e - f - g;
    }

    // multiplb
    public String stringMultiplb() {
        return "ANS Multiplb :";
    }

    public double getMultiplb() {
        return a * b * c * d * e * f * g;
    }

    // divide
    public String stringDivide() {
        return "ANS Divide :";
    }

    public double getDivide() {
        return f / a;
    }

    // mod
    public String stringMod() {
        return "ANS Modulo :";
    }

    public double getMod() {
        return e % b;
    }
}
